import ballerinax/kafka;
import ballerina/io;
import ballerina/log;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

//Setting Paths to JSON files
public string studentInfoPath = "./studentInfo.json";
public string courseInfoPath = "./courseInfo.json";
public string hodInfoPath = "./hodInfo.json";
public string lecturerInfoPath = "./lecturerInfo.json";

//Assigning Paths to variables
public json|io:Error JSONstudentInfo = io:fileReadJson(studentInfoPath);
public json|io:Error JSONlecturerInfo = io:fileReadJson(lecturerInfoPath);
public json|io:Error JSONhodInfo = io:fileReadJson(hodInfoPath);
public json|io:Error JSONcourseInfo = io:fileReadJson(courseInfoPath);

public json courseInfoContent = {
    "math": 
        {  
            "courseName": "math",
            "courseId": "1",
            "department": "mathematics",
            "approved": true
        },
    "programming":  {  
            "courseName": "programming",
            "courseId": "2",
            "department": "informatics",
            "approved": false
        },
    "distributed systems": {  
            "courseName": "distributed systems",
            "courseId": "2",
            "department": "informatics",
            "approved": false
        }
};
public json hodInfoContent = {
    "1092": 
        {  
            "HODName": "Paul",
            "HODId": "1",
            "HODPassword": "12345",
            "department": "informatics"
        },
    "2412":   
        {  
            "HODName": "Jack",
            "HODId": "2",
            "HODPassword": "12345",
            "department": "mathematics"
        }
    
};
public json studentInfoContent = {
    "1234": 
        {  
            "studentName": "Edilson",
            "studentId": "1",
            "studentPassword": "12345",
            "courses": [
                "math", "programming", "distributed systems"
            ]
        },
    "1332":
        {
            "studentName": "dummyStudent",
            "studentId": "2",
            "studentPassword": "12345",
            "courses": [
                "math", "distributed systems"
            ]
        }
};
public json lecturerInfoContent = {
    "1212":
        {  
            "lecturerName": "John",
            "lecturerId": "1",
            "lecturerPassword": "12345",
            "courses": [
                "math"
            ]
        },
    "2222":
        {
            "lecturerName": "dummylecturer",
            "lecturerId": "2",
            "lecturerPassword": "12345",
            "courses": [
                "distributed systems"
            ]
        }

};
//making the json fields accessible
public map<json> mpcourseInfoContent = <map<json>>courseInfoContent;
public map<json> mpstudentInfoContent = <map<json>>studentInfoContent;
public map<json> mphodInfoContent = <map<json>>hodInfoContent;
public map<json> mplecturerInfoContent = <map<json>>lecturerInfoContent;

public boolean loop = true;
public function userAccessField() returns string{
    //authentication
    io:println("Are you a student, lecturer or hod?: ");
    string userChoice = io:readln("");
    if (userChoice === "student"){
        io:println("Enter your student id key: ");
        string userid = io:readln();
        io:print("user successfully logged in");
        userChoice = "student";
        
    } else if (userChoice === "hod") {
        io:println("Enter your hod id key: ");
        string userid = io:readln();
        io:print("user successfully logged in");
        userChoice = "hod";
    }
    else if (userChoice === "lecturer") {
        io:println("Enter your lecturer id key: ");
        string userid = io:readln();
        io:print("user successfully logged in");
        userChoice = "lecturer";
    }
    return userChoice;
}

public function main() {
    string user = userAccessField();

    match user{
        "student" => {
            while loop {
                io:println("Would you like to view a course outline? (y/n)");
                string readOutline = io:readln("");
                if (readOutline === "y") {
                    io:println("Enter course name");
                    string course = io:readln();
                    string message = "";
                    match course {
                        "math" => {
                            message = mpcourseInfoContent["math"].toString();
                        }
                        "programming" => {
                            message = mpcourseInfoContent["programming"].toString();
                        }
                        "distributed systems" => {
                            message = mpcourseInfoContent["distributed systems"].toString();
                        }
                    }
                    
                    check kafkaProducer->send({
                                    topic: "get-course-info",
                                    value: message.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Course sent succesfully");
                    loop = false;
                } else if (readOutline === "n") {
                    // Flushes the sent messages.
                    check kafkaProducer->'flush();
                    loop = false;
                }
                else {
                    loop = false;
                }
            } on fail var e {
            	log:printError("Error occured", e);
            } 
        }
        "hod" => {
            while loop {
                io:println("what would you like to do?\n1. View students\n2. View lecturers\n3. Get Course Info\n4. Sign Course outline");
                string hodChoice = io:readln();

                match hodChoice {
                    "1" => {
                        string students = mpstudentInfoContent.toString();
                        check kafkaProducer->send({
                                    topic: "get-student-info",
                                    value: students.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Students sent succesfully");
                        check kafkaProducer->'flush();
                    loop = false;
                    }
                    "2" => {
                        string lecturer = mplecturerInfoContent.toString();
                        check kafkaProducer->send({
                                    topic: "get-lecturer-info",
                                    value: lecturer.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Lecturers sent succesfully");
                        check kafkaProducer->'flush();
                    }
                    "3" => {
                        io:println("Enter the name of the course you want to view");
                        string course = io:readln();

                        string message = "";
                        match course {
                            "math" => {
                                message = mpcourseInfoContent["math"].toString();
                            }
                            "programming" => {
                                message = mpcourseInfoContent["programming"].toString();
                            }
                            "distributed systems" => {
                                message = mpcourseInfoContent["distributed systems"].toString();
                            }
                        }
                        check kafkaProducer->send({
                                    topic: "get-course-info",
                                    value: message.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Course sent succesfully");
                        loop = false;
                    }
                    "4" => {

                    }
                } on fail var e {
                	log:printError("Error occured", e);
                }
            }
        }
        "lecturer" => {
            while loop {
                io:println("what would you like to do?\n1. View students\n2. Get Course Info\n3. Sign Course outline");
                string hodChoice = io:readln();

                match hodChoice {
                    "1" => {
                        string students = mpstudentInfoContent.toString();
                        check kafkaProducer->send({
                                    topic: "get-student-info",
                                    value: students.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Students sent succesfully");
                        check kafkaProducer->'flush();
                    loop = false;
                    }
                    "2" => {
                        io:println("Enter the name of the course you want to view");
                        string course = io:readln();

                        string message = "";
                        match course {
                            "math" => {
                                message = mpcourseInfoContent["math"].toString();
                            }
                            "programming" => {
                                message = mpcourseInfoContent["programming"].toString();
                            }
                            "distributed systems" => {
                                message = mpcourseInfoContent["distributed systems"].toString();
                            }
                        }
                        check kafkaProducer->send({
                                    topic: "get-course-info",
                                    value: message.toBytes() });
                                    //Acknowledgment
                                    log:printInfo("Course sent succesfully");
                        loop = false;
                    }
                    "3" => {
                        
                    }
                    
                } on fail var e {
                	log:printError("Error occured", e);
                }
            }
        }
    }
}







// public function main() returns error? {
//     string message = "Hello World, Ballerina";
//     // Sends the message to the Kafka topic.
//     check kafkaProducer->send({
//                                 topic: "test-kafka-topic",
//                                 value: message.toBytes() });

//     // Flushes the sent messages.
//     check kafkaProducer->'flush();
    
// }
