# DSA Assignment 3 Documentation

**By Edilson Zau - 220090491** 


This project aimed to design and implement a distributed course outline automation. 

The system should contain at least one producer (server) and one consumer (client). 



## Its key functions should include: 

* Viewing course outlines
* Viewing information about a course
* Viewing information about students and lecturers
* Creating a course outline
* Adding a digital signiture to a course outline


To run the files, Kafka should already be downloaded on the system. 

---

## Kafka Setup 

The consumer and producer need to be able to communicate through messages shared via a Kafka cluster. 

To run the files, Kafka should already be installed on the system. Zookeeper and Kafka Broker should be run. 

### Running Zookeeper 

Use the following command on the bin directory of Kafka to start zookeeper:
>zookeeper-server-start.bat ..\\..\config\zookeeper.properties


### Running Kafka-Broker

Use the following command on the bin directory of Kafka to start Kafka-Broker:

>kafka-server-start.bat ..\\..\config\server.properties

<!-- Once Zookeeper and Kafka-broker are running, the files can now be executed. -->


*Note it will not be necessary to run the consumer and producer in the Kafka setup, as it will be done in the ballerina files.*

---

## Ballerina Setup

Follow the instructions below to run the consumer and producer. 

*Note for the files to run successfully please run the consumer before the producer.*

### Running The Consumer 

Use the following command on the terminal to run the producer: 

>bal run consumer/

*Note you need to be inside the directory **'dsa-assignment-3'** for the commands to function.*

### Running The Producer 

Use the following command on the terminal to run the producer: 

>bal run producer/

*Note you need to be inside the directory **'dsa-assignment-3'** for the commands to function.*


--- 

## Deployment

The deployment can be created running the following command: 
> kubectl apply -f openApi.yml

Use the command below to see if deployment was succesful:
> kubectl get deployment.apps/dsa-assignment-3

and 

> kubectl get service

Access the service deployed using: 
> minikube service dsa-assignment-3-service


*NOTE kubectl and minikube need to be installed in the system for the above commands to run*
*Note a kubernetes engine needs to be running for the commands to run*
